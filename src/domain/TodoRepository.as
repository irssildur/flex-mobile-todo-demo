package domain
{
	import mx.collections.ArrayList;

	public class TodoRepository
	{
		private static var instance:TodoRepository;
		
		public var todos:ArrayList = new ArrayList();
		
		public function TodoRepository()
		{
			
		}
		
		public static function getInstance():TodoRepository
		{
			if (instance == null) {
				instance = new TodoRepository();
			}
			return instance;
		}
	}
}