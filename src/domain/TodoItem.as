package domain
{
	public class TodoItem
	{
		public var name:String;
		public var description:String;
		public var priority:String;
		public var done:Boolean;
	}
}